#Contents

This repository contains five files. The file `codek.sage` is the file that has the suitable weighted adjacency matrix for the `k`-setwise intersecting permutations, for $k\in \{3,4,5\}$. The files `4-setwise.ipynb` and `5-setwise.ipynb` are Jupyter notebook files that have the necessary computations for the small eigenvalues. 

# How to use this code

First, replace the first line of the .sage files with
`#!name`, where name is the output of `which sagemath`. Then make the files executable with `chmod +x name-of-the-file.sage`.



To use the code, simply run `./name-of-the-file.sage n option`, where $n$ is a positive integer and option is `True` or `False`. If `option` is set to `True`, then the each eigenvalues from an irreducible character will be printed.
