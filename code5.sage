#!/usr/local/bin/sage

#To run this code, run the command chmod +x three-setwise.sage in the terminal. Then run the command ./three-setwise.sage n key


import sys

def is_derangement(x,P): #input a permutation x and a set P of all partitions of the integer 3 (consisting of)
    u = Permutation(x).cycle_type()
    for y in P:
        if is_submultiset(y,u) == True:
            return False
    return True

def is_submultiset(A,B): #Checks whether A is a submultiset of B
    B = list(B)
    n = len(A)
    a = True
    i = 0
    while i<n and a == True:
        u = A[i]
        if u in B:
            B.remove(u)
        else:
            a = False
        i += 1
    return a

def VA(m):  #initialize the variables that are used in the weightings on the conjugacy classes of Sym(n)
      t = ''
      for i in range(m):
          if i == m-1:
              t += 'X{0}'.format(i)
          else:
              t += 'X{0},'.format(i)
      return var(t)


# n = argument of Sym(n), k = 3, L = a set of conjugacy classes (as partitions of n), M = a set of partitions of n whose corresponding eigenvalue is -1.
#This function checks whether there is a solution to the system of linear equation of conjugacy classes in L and irreducible characters in M.

def weights(n,k,L,M):
    P = Partitions(k)
    G = SymmetricGroup(n)
    l,m = len(L),len(M)   #L = conjugacy class and M irreducible characters
    #print l,m
    V = VA(m)
    CC_size = []
    char_values = []
    Eq = []
    for F in L:
        for x in SymmetricGroup(n).conjugacy_classes_representatives():
            if x.cycle_type() == F:
                CC_size.append([SymmetricGroup(n).conjugacy_class(x).cardinality(),x])
                break
            else:
                pass
    char = [SymmetricGroupRepresentation(M[i]) for i in range(m)]
    print "--------------------------------------------------------------------------------------------------"
    for chi in char:
        phi = chi#.to_character()
        t = 0
        for i in range(m):
            t += V[i]*phi(CC_size[i][1]).trace()*CC_size[i][0]
            print chi,V[i],phi(CC_size[i][1]).trace(),CC_size[i][1].cycle_type()
        if phi == SymmetricGroupRepresentation([n]):
            Eq.append(t == binomial(n,k) -1)  #The maximum eigenvalue is set to be C(n,k) - 1
        else:
            Eq.append(t == -phi(G[0]).trace())  #The eigenvalue corresponding to phi is set to be equal to -1
        print "---------------------------------------------------------------------------------------------------"
    solutions = solve(Eq,V)
    print "\n","Weight possible?", len(solutions) != 0,"\n"
    if len(solutions) == 0:
        return sys.exit()
##################################################################
    derangement_conjugacy_classes_representatives = []
    CC = G.conjugacy_classes_representatives()
    for x in CC:
        if is_derangement(x,P) == True:
            derangement_conjugacy_classes_representatives.append(x)
    Q = derangement_conjugacy_classes_representatives
    #print len(Q)
    W = [0]*len(Q)
    for i in range(len(Q)):
        u = Q[i].cycle_type()
        #print u
        for j in range(m):
            if u == Partition(L[j]):
                W[i] = solutions[0][j].rhs()
    print "System of linear equations","\n"
    for x in Eq:
        print x
    print "------------------------------------------------------------------------------------------------","\n"
    print "Weights"
    for x in solutions[0]:
        print x
    print "------------------------------------------------------------------------------------------------","\n"
    return [solutions[0][i].rhs() for i in range(len(solutions[0]))],W #W is the appropriate wgenvalues_weightshtings for the conjugacy classes of derangements

# This function takes a weighting W of the conjugacy classes of derangements and produces eigenvalues of the corresponding weighted spanning subgraph.
# The argument W is the weights of the conjugacy classes of derangements
# key is an option to print the eigenvalues during the computation

def eigenvalues_weights(n,k,W,key):
    print "Computing the eigenvalues ...",'\n',"this might take a few minutes",'\n'

    P = Partitions(k)
    eigenvalues = []
    G = SymmetricGroup(n)
    derangement_conjugacy_classes_representatives = []
    CC = G.conjugacy_classes_representatives()
    for x in CC:
        if is_derangement(x,P) == True:
            derangement_conjugacy_classes_representatives.append(x)
    Irr = G.irreducible_characters()
    for psi in Irr:
        s = 0
        L = derangement_conjugacy_classes_representatives
        for i in range(len(L)):
            s += (1/psi.degree()) * ( W[i]*G.conjugacy_class(L[i]).cardinality()*psi(L[i]) )
        eigenvalues.append(s)
        if key == 'true':
            print s
        elif key == 'false':
            pass
        else:
            print "Stoped! Set key = true or key = false"
            return sys.exit()

    print "Eigenvalues","\n"
    return eigenvalues

def eigenvalues_weights_with_conjugacy_classes(n,k,W,Q,key):
    print "Computing the eigenvalues ...",'\n',"this might take a few minutes",'\n'

    P = Partitions(k)
    eigenvalues = []
    G = SymmetricGroup(n)
    Irr = G.irreducible_characters()
    for psi in Irr:
        if psi.degree() < binomial(n,k+1):
            s = 0
            L = Q
            for i in range(len(L)):
                s += (1/psi.degree()) * ( W[i]*G.conjugacy_class(L[i]).cardinality()*psi(L[i]) )

            eigenvalues.append(s)
            if key == 'true':
                print s
            elif key == 'false':
                pass
            else:
                print "Stoped! Set key = true or key = false"
                return sys.exit()

    print "Eigenvalues","\n"
    print "Eigenvalues corresponding to low dimensional irreducible characters","\n"
    print 'The largest eigenvalue is ',max(eigenvalues),'\n'
    print 'The least eigenvalue is ',min(eigenvalues),'\n'
    print "--------------------------------------------------",'\n'
    return eigenvalues

def is_Derangement(x):
     if len(Permutation(x).fixed_points()) == 0:
         return True
     else:
         return False

def der_graph(G):
    CC = G.conjugacy_classes_representatives()
    D = []
    for x in CC:
        if is_Derangement(x) == True:
            D += G.conjugacy_class(x).list()
    return Graph(G.cayley_graph(generators = D))

#######################
n = int(sys.argv[1])
key = sys.argv[2] # true or false
k = 5
G = SymmetricGroup(n)
#######################

def modified_weights(n,k,L,M,Phi):
    #print Phi
    P = Partitions(k)
    l,m = len(L),len(M)   #L = conj and M char
    print l,m
    V = VA(l)
    var('z')
    CC_size = []
    char_values = []
    Eq = []
    for F in L:
        for x in SymmetricGroup(n).conjugacy_classes_representatives():
            if x.cycle_type() == Partition(F):
                CC_size.append([SymmetricGroup(n).conjugacy_class(x).cardinality(),x.cycle_type()])
                break
            else:
                pass
    char = [SymmetricGroupRepresentation(M[i]) for i in range(m)]
    T = []
    for j in range(m):
        t = 0
        for i in range(l):
            t += V[i]*Phi[j][i]*CC_size[i][0]
        T.append(t)
    for i in range(m):
        if i == 0:
            Eq.append(T[i] == binomial(n,k) -1)  ######## maximum eigenvalue
            #print Eq[-1]
        elif M[i] == [n-i,i]:
            Eq.append(T[i] == -binomial(n,i) + binomial(n,i-1))
        elif M[i] == [n-3,2,1]:
            Eq.append(T[i] == -n*(n-2)*(n-4)/3)
        elif M[i] == [n-3,1,1,1]:
            Eq.append(T[i] == -binomial(n-1,3))
        elif M[i] == [n-2,1,1]:
            Eq.append(T[i] == -binomial(n-1,2))
        elif M[i] == [n-4,3,1]:
            Eq.append(T[i] == -n*(n-1)*(n-3)*(n-6)/8)
        elif M[i] == [n-4,1,1,1,1]:
            Eq.append(T[i] == -binomial(n-1,4))
        elif M[i] == [n-4,2,1,1]:
            Eq.append(T[i] == -n*(n-2)*(n-3)*(n-5)/8)
        elif M[i] == [n-4,2,2]:
            Eq.append(T[i] == -1* n*(n-1)*(n-4)*(n-5)/12)
    print "\n\n"
    Sol = solve(Eq,V)
    print Eq
    print "Weight possible?", len(Sol) != 0
    if len(Sol) == 0:
        return sys.exit()
##################################################################
    G = SymmetricGroup(n)
    derangement_conjugacy_classes_representatives = []
    CC = G.conjugacy_classes_representatives()
    for x in CC:
        if is_derangement(x,P) == True:
            derangement_conjugacy_classes_representatives.append(x)
    Q = derangement_conjugacy_classes_representatives
    print len(Q)
    W = [0]*len(Q)
    for i in range(len(Q)):
        u = Q[i].cycle_type()
        #print u
        for j in range(l):
            if u == Partition(L[j]):
                W[i] = Sol[0][j].rhs()
    #print "Equation",Eq
    print "\n",Sol,"\n"
    return [Sol[0][i].rhs() for i in range(len(Sol[0]))],W,Q

def find_table(m,L,M):
    #print len(L),len(M)
    T = []
    G = SymmetricGroup(m)
    Irr = G.irreducible_characters()
    P = Partitions(m)
    CC = G.conjugacy_classes_representatives()
    C = []
    for x in L:
     for y in CC:
         if x == list(y.cycle_type()):
             C.append(y)
    Index = []
    for x in M:
        for i in range(len(P)):
            if Partition(x) == P[i]:
                Index.append(i)
    for i in [len(Irr)-1-j for j in Index]:
        phi = Irr[i]
        T.append([phi(x) for x in C])#,P[len(P)-1-i]
    return T

if n in [11,13]:
    PC = [[n],[n-1,1],[n-2,2],[n-3,3],[n-4,4],[n-5,5]]
    L = [[n-4,1,1,1,1],[n-7,4,3],[n-3,2,1],[n],[n-2,1,1]]
    print find_table(n,L,PC)
    W = modified_weights(n,5,L,PC,find_table(n,L,PC))
    L =  eigenvalues_weights_with_conjugacy_classes(n,k,W[1],W[2],key)
    print L

if n in [15,17,19]:
    PC = [[n],[n-1,1],[n-2,2],[n-3,3],[n-4,4],[n-5,5],[n-2,1,1]]
    L = [[n],[n-2,1,1],[n-3,2,1],[n-4,2,2],[n-4,1,1,1,1],[n-4,3,1]]
    print find_table(n,L,PC)
    W = modified_weights(n,5,L,PC,find_table(n,L,PC))
    L =  eigenvalues_weights_with_conjugacy_classes(n,k,W[1],W[2],key)
    print L

if is_even(n) == True and n in [12]:
    PC = [[n],[n-1,1],[n-2,2],[n-3,3],[n-4,4],[n-5,5],[n-3,2,1]]
    L = [[n-1,1],[n-2,2],[n-3,3],[n-3,1,1,1],[n-4,4],[n-8,4,2,2]]
    print find_table(n,L,PC)
    W = modified_weights(n,5,L,PC,find_table(n,L,PC))
    L =  eigenvalues_weights_with_conjugacy_classes(n,k,W[1],W[2],key)
    print L

if is_even(n) == True and n in [14]:
    PC = [[n],[n-1,1],[n-2,2],[n-3,3],[n-4,4],[n-5,5],[n-3,2,1],[n-3,1,1,1]]
    L = [[n-1,1],[n-2,2],[n-3,3],[n-3,1,1,1],[n-4,4],[n-4,2,1,1],[n-6,6]]
    print find_table(n,L,PC)
    W = modified_weights(n,5,L,PC,find_table(n,L,PC))
    L =  eigenvalues_weights_with_conjugacy_classes(n,k,W[1],W[2],key)
    print L

if is_even(n) == True and n in [16]:
    PC = [[n],[n-1,1],[n-2,2],[n-3,3],[n-4,4],[n-5,5]]
    L = [[n-1,1],[n-2,2],[n-3,3],[n-3,1,1,1],[n-4,4],[n-4,2,1,1],[n-7,7],[n-13, 3, 3, 3, 3, 1]]
    print find_table(n,L,PC)
    W = modified_weights(n,5,L,PC,find_table(n,L,PC))
    L =  eigenvalues_weights_with_conjugacy_classes(n,k,W[1],W[2],key)
    var('r1,r2,r3')
    print [ x.substitute(r1 = (1/798336000), r2 = (-1/243302400), r3 = (1/465696000)) for x in L]

if is_even(n) == True and n in [18]:
    PC = [[n],[n-1,1],[n-2,2],[n-3,3],[n-4,4],[n-5,5],[n-2,1,1],[n-3,2,1],[n-4,1,1,1,1]]
    L = [[n-1,1],[n-2,2],[n-3,3],[n-3,1,1,1],[n-4,4],[n-4,2,1,1],[n-7,7],[n-7,3,3,1]]
    print find_table(n,L,PC)
    W = modified_weights(n,5,L,PC,find_table(n,L,PC))
    L =  eigenvalues_weights_with_conjugacy_classes(n,k,W[1],W[2],key)
    print L

if is_even(n) == True and n in [20]:
    PC = [[n],[n-1,1],[n-2,2],[n-3,3],[n-4,4],[n-5,5],[n-3,2,1],[n-2,1,1],[n-3,1,1,1],[n-4,3,1],[n-4,2,2],[n-4,2,1,1],[n-4,1,1,1,1]]
    L = [
[10, 6, 1, 1, 1, 1],
[10, 10],
[11, 7, 1, 1],
[12, 4, 2, 2],
[13, 3, 3, 1],
[16, 2, 1, 1],
[16, 4],
[17, 1, 1, 1],
[19, 1],
]
    print find_table(n,L,PC)
    W = modified_weights(n,5,L,PC,find_table(n,L,PC))
    L =  eigenvalues_weights_with_conjugacy_classes(n,k,W[1],W[2],key)
    print L

elif is_even(n) == True and n >= 22:
    """ ORDER THE CHARACTERS PROPERLY"""
    PC = [[n],[n-1,1],[n-2,2],[n-3,3],[n-4,4],[n-5,5],[n-2,1,1],[n-3,2,1],[n-4,3,1],[n-4,2,2],[n-4,2,1,1],[n-4,1,1,1,1],[n-3,1,1,1]] #add [n-3,1,1,1] in the past version
    L = [[n-1,1],[n-2,2],[n-3,1,1,1],[n-4,4],[n-4,2,1,1],[n-6,6],[n-6,2,2,2],[n-7,3,3,1],[n-8,6,1,1],[n-10,6,1,1,1,1]]
    X = find_table(n,L,PC)
    print X
    W = modified_weights(n,5,L,PC,find_table(n,L,PC))
elif is_even(n) == False and n >= 21:
    """ ORDER THE CHARACTERS PROPERLY"""
    PC = [[n],[n-1,1],[n-2,2],[n-3,3],[n-4,4],[n-5,5],[n-2,1,1],[n-3,2,1],[n-4,3,1],[n-4,2,2],[n-4,2,1,1],[n-4,1,1,1,1]] #permutation_characters
    L = [[n],[n-2,1,1],[n-3,2,1],[n-4,2,2],[n-4,1,1,1,1],[n-4,3,1],[n-7,6,1],[n-8,4,4],[n-9,6,1,1,1]]
    print find_table(n,L,PC)
    W = modified_weights(n,5,L,PC,find_table(n,L,PC))
    print W[1],"\n"

    L =  eigenvalues_weights_with_conjugacy_classes(n,k,W[1],W[2],key)
    print L
