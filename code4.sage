#!/usr/local/bin/sage

#To run this code, run the command chmod +x three-setwise.sage in the terminal. Then run the command ./three-setwise.sage n key


import sys

def is_derangement(x,P): #input a permutation x and a set P of all partitions of the integer 3 (consisting of)
    u = Permutation(x).cycle_type()
    for y in P:
        if is_submultiset(y,u) == True:
            return False
    return True

def is_submultiset(A,B): #Checks whether A is a submultiset of B
    B = list(B)
    n = len(A)
    a = True
    i = 0
    while i<n and a == True:
        u = A[i]
        if u in B:
            B.remove(u)
        else:
            a = False
        i += 1
    return a

def VA(m):  #initialize the variables that are used in the weightings on the conjugacy classes of Sym(n)
      t = ''
      for i in range(m):
          if i == m-1:
              t += 'X{0}'.format(i)
          else:
              t += 'X{0},'.format(i)
      return var(t)


# n = argument of Sym(n), k = 3, L = a set of conjugacy classes (as partitions of n), M = a set of partitions of n whose corresponding eigenvalue is -1.
#This function checks whether there is a solution to the system of linear equation of conjugacy classes in L and irreducible characters in M.

def weights(n,k,L,M):
    P = Partitions(k)
    G = SymmetricGroup(n)
    l,m = len(L),len(M)   #L = conjugacy class and M irreducible characters
    #print l,m
    V = VA(m)
    CC_size = []
    char_values = []
    Eq = []
    for F in L:
        for x in SymmetricGroup(n).conjugacy_classes_representatives():
            if x.cycle_type() == F:
                CC_size.append([SymmetricGroup(n).conjugacy_class(x).cardinality(),x])
                break
            else:
                pass
    char = [SymmetricGroupRepresentation(M[i]) for i in range(m)]
    print "--------------------------------------------------------------------------------------------------"
    for chi in char:
        phi = chi#.to_character()
        t = 0
        for i in range(m):
            t += V[i]*phi(CC_size[i][1]).trace()*CC_size[i][0]
            print chi,V[i],phi(CC_size[i][1]).trace(),CC_size[i][1].cycle_type()
        if phi == SymmetricGroupRepresentation([n]):
            Eq.append(t == binomial(n,k) -1)  #The maximum eigenvalue is set to be C(n,k) - 1
        else:
            Eq.append(t == -phi(G[0]).trace())  #The eigenvalue corresponding to phi is set to be equal to -1
        print "---------------------------------------------------------------------------------------------------"
    solutions = solve(Eq,V)
    print "\n","Weight possible?", len(solutions) != 0,"\n"
    if len(solutions) == 0:
        return sys.exit()
##################################################################
    derangement_conjugacy_classes_representatives = []
    CC = G.conjugacy_classes_representatives()
    for x in CC:
        if is_derangement(x,P) == True:
            derangement_conjugacy_classes_representatives.append(x)
    Q = derangement_conjugacy_classes_representatives
    #print len(Q)
    W = [0]*len(Q)
    for i in range(len(Q)):
        u = Q[i].cycle_type()
        #print u
        for j in range(m):
            if u == Partition(L[j]):
                W[i] = solutions[0][j].rhs()
    print "System of linear equations","\n"
    for x in Eq:
        print x
    print "------------------------------------------------------------------------------------------------","\n"
    print "Weights"
    for x in solutions[0]:
        print x
    print "------------------------------------------------------------------------------------------------","\n"
    return [solutions[0][i].rhs() for i in range(len(solutions[0]))],W #W is the appropriate wgenvalues_weightshtings for the conjugacy classes of derangements

# This function takes a weighting W of the conjugacy classes of derangements and produces eigenvalues of the corresponding weighted spanning subgraph.
# The argument W is the weights of the conjugacy classes of derangements
# key is an option to print the eigenvalues during the computation

def eigenvalues_weights(n,k,W,key):
    print "Computing the eigenvalues ...",'\n',"this might take a few minutes",'\n'

    P = Partitions(k)
    eigenvalues = []
    G = SymmetricGroup(n)
    derangement_conjugacy_classes_representatives = []
    CC = G.conjugacy_classes_representatives()
    for x in CC:
        if is_derangement(x,P) == True:
            derangement_conjugacy_classes_representatives.append(x)
    Irr = G.irreducible_characters()#
    for psi in Irr:
        s = 0
        L = derangement_conjugacy_classes_representatives
        for i in range(len(L)):
            s += (1/psi.degree()) * ( W[i]*G.conjugacy_class(L[i]).cardinality()*psi(L[i]) )
        eigenvalues.append(s)
        if key == 'true':
            print s
        elif key == 'false':
            pass
        else:
            print "Stoped! Set key = true or key = false"
            return sys.exit()

    print "Eigenvalues","\n"
    return eigenvalues

def eigenvalues_weights_with_conjugacy_classes(n,k,W,Q,key):
    print "Computing the eigenvalues ...",'\n',"this might take a few minutes",'\n'

    P = Partitions(k)
    eigenvalues = []
    G = SymmetricGroup(n)
    Irr = G.irreducible_characters()#
    #Irr = SymmetricGroupRepresentations(n)
    for psi in Irr:
        #phi = psi.to_character()
        if psi.degree() < binomial(n,5):
            s = 0
            L = Q
            for i in range(len(L)):
                s += (1/psi.degree()) * ( W[i]*G.conjugacy_class(L[i]).cardinality()*psi(L[i]) )

            eigenvalues.append(s)
            if key == 'true':
                print s
            elif key == 'false':
                pass
            else:
                print "Stoped! Set key = true or key = false"
                return sys.exit()

    print "Eigenvalues corresponding to low dimensional irreducible characters","\n"
    print 'The largest eigenvalue is ',max(eigenvalues),'\n'
    print 'The least eigenvalue is ',min(eigenvalues),'\n'
    print "--------------------------------------------------",'\n'

    return eigenvalues

def is_Derangement(x):
     if len(Permutation(x).fixed_points()) == 0:
         return True
     else:
         return False

def der_graph(G):
    CC = G.conjugacy_classes_representatives()
    D = []
    for x in CC:
        if is_Derangement(x) == True:
            D += G.conjugacy_class(x).list()
    return Graph(G.cayley_graph(generators = D))

def label_subsets(n,k):
    L = Combinations([1..n],k)
    D = dict()
    i = 1
    for x in L:
        D[tuple(x)] = i
        i += 1
    return D,L


def action_of_element_as_permutation_on_subsets(k,f,T):
    u = tuple([f(T[i]) for i in [0..k-1]])
    return sorted(u)


def group_acting_on_subsets(G,k):
    PermGens = []
    n = G.degree()
    S = G.gens()
    K = label_subsets(n,k)
    L = K[1]
    D = K[0]
    for g in S:
        N = [0]*binomial(n,k)
        for x in L:
            i = D[tuple(x)]
            j = D[tuple(action_of_element_as_permutation_on_subsets(k,g,x))]
            N[i-1] = j
        PermGens.append(Permutation(N))
    return PermutationGroup(PermGens)

#######################
n = int(sys.argv[1])
key = sys.argv[2] # true or false
k = 4
G = SymmetricGroup(n)
#######################

def modified_weights(n,k,L,M,Phi):
    #print Phi
    P = Partitions(k)
    l,m = len(L),len(M)   #L = conj and M char
    #print l,m
    V = VA(l)
    var('z')
    CC_size = []
    char_values = []
    Eq = []
    for F in L:
        for x in SymmetricGroup(n).conjugacy_classes_representatives():
            if x.cycle_type() == Partition(F):
                CC_size.append([SymmetricGroup(n).conjugacy_class(x).cardinality(),x.cycle_type()])
                break
            else:
                pass
    char = [SymmetricGroupRepresentation(M[i]) for i in range(m)]
    #print CC_size
    T = []
    for j in range(m):
        t = 0
        for i in range(l):
            t += V[i]*Phi[j][i]*CC_size[i][0]
        T.append(t)
    for i in range(m):
        if i == 0:
            Eq.append(T[i] == binomial(n,k) -1)  ######## maximum eigenvalue
            #print Eq[-1]
        elif M[i] == [n-i,i]:
            Eq.append(T[i] == -binomial(n,i) + binomial(n,i-1))
        elif M[i] == [n-3,2,1]:
            Eq.append(T[i] == -n*(n-2)*(n-4)/3)
        elif M[i] == [n-3,1,1,1]:
            Eq.append(T[i] == -binomial(n-1,3))
        elif M[i] == [n-2,1,1]:
            Eq.append(T[i] == -binomial(n-1,2))
    print "\n\n"
    Sol = solve(Eq,V)
    print Eq
    print "Weight possible?", len(Sol) != 0
    if len(Sol) == 0:
        return sys.exit()
##################################################################
    G = SymmetricGroup(n)
    derangement_conjugacy_classes_representatives = []
    CC = G.conjugacy_classes_representatives()
    for x in CC:
        if is_derangement(x,P) == True:
            derangement_conjugacy_classes_representatives.append(x)
    Q = derangement_conjugacy_classes_representatives
    W = [0]*len(Q)
    for i in range(len(Q)):
        u = Q[i].cycle_type()
        for j in range(l):
            if u == Partition(L[j]):
                W[i] = Sol[0][j].rhs()
    print "\n",Sol,"\n"
    return [Sol[0][i].rhs() for i in range(len(Sol[0]))],W,Q

def find_table(m,L,M):
    T = []
    G = SymmetricGroup(m)
    Irr = G.irreducible_characters()
    P = Partitions(m)
    CC = G.conjugacy_classes_representatives()
    C = []
    for x in L:
     for y in CC:
         if x == list(y.cycle_type()):
             C.append(y)
    Index = []
    for x in M:
        for i in range(len(P)):
            if Partition(x) == P[i]:
                Index.append(i)
    for i in [len(Irr)-1-j for j in Index]:
        phi = Irr[i]
        T.append([phi(x) for x in C])
    return T


if is_even(n) == True and n not in [10]:
    PC = [[n],[n-1,1],[n-2,2],[n-3,3],[n-4,4],[n-3,2,1]]
    L = [[n-1,1],[n-2,2],[n-3,3],[n-3,1,1,1],[n-7,5,1,1]]
    print find_table(n,L,PC)
    W = modified_weights(n,4,L,PC,find_table(n,L,PC))
    L =  eigenvalues_weights_with_conjugacy_classes(n,k,W[1],W[2],key)
    print L

elif n == 9:
        G = SymmetricGroup(n)
        K = group_acting_on_subsets(G,4)
        MK = K.conjugacy_classes_subgroups()
        x = MK[539]
        X = der_graph(x)
        print x.structure_description(),"is a transitive sugroup whose intersection density is equal to",X.independent_set(value_only=True)/x.stabilizer(1).order()

elif n == 10:
    """ ORDER THE CHARACTERS PROPERLY"""
    PC = [[n],[n-1,1],[n-2,2],[n-3,3],[n-4,4]]
    L = [[n-1,1],[n-2,2],[n-3,3],[n-3,1,1,1]]
    print find_table(n,L,PC)
    W = modified_weights(n,4,L,PC,find_table(n,L,PC))
    L =  eigenvalues_weights_with_conjugacy_classes(n,k,W[1],W[2],key)

elif n  == 11:
    """ ORDER THE CHARACTERS PROPERLY"""
    PC = [[n],[n-1,1],[n-2,2],[n-3,3],[n-2,1,1]] #permutation_characters
    L = [[n],[n-2,1,1],[n-3,2,1],[n-5,3,2]]
    print find_table(n,L,PC)
    W = modified_weights(n,4,L,PC,find_table(n,L,PC))
    print W[1],"\n"
    L =  eigenvalues_weights_with_conjugacy_classes(n,k,W[1],W[2],key)
    print L

elif n  == 13:
    """ ORDER THE CHARACTERS PROPERLY"""
    PC = [[n],[n-1,1],[n-2,2],[n-3,3],[n-2,1,1]] #permutation_characters
    L = [[n],[n-3,2,1],[n-2,1,1],[5,5,3],[8,3,2]]
    print find_table(n,L,PC)
    W = modified_weights(n,4,L,PC,find_table(n,L,PC))
    print W[1],"\n"
    L =  eigenvalues_weights_with_conjugacy_classes(n,k,W[1],W[2],key)
    print L

elif is_even(n) == False and n not in [9,11,13]:
    """ ORDER THE CHARACTERS PROPERLY"""
    PC = [[n],[n-1,1],[n-2,2],[n-3,3],[n-3,2,1],[n-2,1,1]] #permutation_characters
    L = [[n],[n-2,1,1],[n-3,2,1],[n-6,3,3],[n-7,6,1],[n-9,6,1,1,1]]
    print find_table(n,L,PC)
    W = modified_weights(n,4,L,PC,find_table(n,L,PC))
    print W[1],"\n"
    L =  eigenvalues_weights_with_conjugacy_classes(n,k,W[1],W[2],key)
    print L
