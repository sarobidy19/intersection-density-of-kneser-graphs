#!/usr/local/bin/sage

#To run this code, run the command chmod +x three-setwise.sage in the terminal. Then run the command ./three-setwise.sage n key


import sys

def is_derangement(x,P): #input a permutation x and a set P of all partitions of the integer 3 (consisting of)
    u = Permutation(x).cycle_type()
    for y in P:
        if is_submultiset(y,u) == True:
            return False
    return True

def is_submultiset(A,B): #Checks whether A is a submultiset of B
    B = list(B)
    n = len(A)
    a = True
    i = 0
    while i<n and a == True:
        u = A[i]
        if u in B:
            B.remove(u)
        else:
            a = False
        i += 1
    return a

def VA(m):  #initialize the variables that are used in the weightings on the conjugacy classes of Sym(n)
      t = ''
      for i in range(m):
          if i == m-1:
              t += 'X{0}'.format(i)
          else:
              t += 'X{0},'.format(i)
      return var(t)


# n = argument of Sym(n), k = 3, L = a set of conjugacy classes (as partitions of n), M = a set of partitions of n whose corresponding eigenvalue is -1.
#This function checks whether there is a solution to the system of linear equation of conjugacy classes in L and irreducible characters in M.

def weights(n,k,L,M):
    P = Partitions(k)
    G = SymmetricGroup(n)
    l,m = len(L),len(M)   #L = conjugacy class and M irreducible characters
    #print l,m
    V = VA(m)
    CC_size = []
    char_values = []
    Eq = []
    for F in L:
        for x in SymmetricGroup(n).conjugacy_classes_representatives():
            if x.cycle_type() == F:
                CC_size.append([SymmetricGroup(n).conjugacy_class(x).cardinality(),x])
                break
            else:
                pass
    char = [SymmetricGroupRepresentation(M[i]) for i in range(m)]
    print "--------------------------------------------------------------------------------------------------"
    for chi in char:
        phi = chi#.to_character()
        t = 0
        for i in range(m):
            t += V[i]*phi(CC_size[i][1]).trace()*CC_size[i][0]
            print chi,V[i],phi(CC_size[i][1]).trace(),CC_size[i][1].cycle_type()
        if phi == SymmetricGroupRepresentation([n]):
            Eq.append(t == binomial(n,k) -1)  #The maximum eigenvalue is set to be C(n,k) - 1
        else:
            Eq.append(t == -phi(G[0]).trace())  #The eigenvalue corresponding to phi is set to be equal to -1
        print "---------------------------------------------------------------------------------------------------"
    solutions = solve(Eq,V)
    print "\n","Weight possible?", len(solutions) != 0,"\n"
    if len(solutions) == 0:
        return sys.exit()
##################################################################
    derangement_conjugacy_classes_representatives = []
    CC = G.conjugacy_classes_representatives()
    for x in CC:
        if is_derangement(x,P) == True:
            derangement_conjugacy_classes_representatives.append(x)
    Q = derangement_conjugacy_classes_representatives
    #print len(Q)
    W = [0]*len(Q)
    for i in range(len(Q)):
        u = Q[i].cycle_type()
        #print u
        for j in range(m):
            if u == Partition(L[j]):
                W[i] = solutions[0][j].rhs()
    print "System of linear equations","\n"
    for x in Eq:
        print x
    print "------------------------------------------------------------------------------------------------","\n"
    print "Weights"
    for x in solutions[0]:
        print x
    print "------------------------------------------------------------------------------------------------","\n"
    return [solutions[0][i].rhs() for i in range(len(solutions[0]))],W

def eigenvalues_weights(n,k,W,key):
    print "Computing the eigenvalues ...",'\n',"this might take a few minutes",'\n'

    P = Partitions(k)
    eigenvalues = []
    G = SymmetricGroup(n)
    derangement_conjugacy_classes_representatives = []
    CC = G.conjugacy_classes_representatives()
    for x in CC:
        if is_derangement(x,P) == True:
            derangement_conjugacy_classes_representatives.append(x)
    Irr = G.irreducible_characters()#
    for psi in Irr:
        s = 0
        L = derangement_conjugacy_classes_representatives
        for i in range(len(L)):
            s += (1/psi.degree()) * ( W[i]*G.conjugacy_class(L[i]).cardinality()*psi(L[i]) )
        eigenvalues.append(s)
        if key == 'true':
            print s
        elif key == 'false':
            pass
        else:
            print "Stoped! Set key = true or key = false"
            return sys.exit()

    print "Eigenvalues","\n"
    return eigenvalues

def eigenvalues_weights_with_conjugacy_classes(n,k,W,Q,key):
    print "Computing the eigenvalues ...",'\n',"this might take a few minutes",'\n'

    P = Partitions(k)
    eigenvalues = []
    G = SymmetricGroup(n)
    Irr = G.irreducible_characters()#
    #Irr = SymmetricGroupRepresentations(n)
    for psi in Irr:
        #phi = psi.to_character()
        if psi.degree() < binomial(n,4):
            s = 0
            L = Q
            for i in range(len(L)):
                s += (1/psi.degree()) * ( W[i]*G.conjugacy_class(L[i]).cardinality()*psi(L[i]) )

            eigenvalues.append(s)
            if key == 'true':
                print s
            elif key == 'false':
                pass
            else:
                print "Stoped! Set key = true or key = false"
                return sys.exit()

    print "Eigenvalues corresponding to low dimensional irreducible characters","\n"
    print 'The largest eigenvalue is ',max(eigenvalues),'\n'
    print 'The least eigenvalue is ',min(eigenvalues),'\n'
    print "--------------------------------------------------",'\n'

    return eigenvalues

#######################
n = int(sys.argv[1])
key = sys.argv[2] # true or false
k = 3
G = SymmetricGroup(n)
#######################

def modified_weights(n,k,L,M,Phi):
    P = Partitions(k)
    l,m = len(L),len(M)   #L = conj and M char
    #print l,m
    V = VA(l)
    var('z')
    CC_size = []
    char_values = []
    Eq = []
    for F in L:
        for x in SymmetricGroup(n).conjugacy_classes_representatives():
            if x.cycle_type() == Partition(F):
                CC_size.append([SymmetricGroup(n).conjugacy_class(x).cardinality(),x.cycle_type()])
                break
            else:
                pass
    char = [SymmetricGroupRepresentation(M[i]) for i in range(m)]
    #print CC_size
    T = []
    for j in range(m):
        t = 0
        for i in range(l):
            t += V[i]*Phi[j][i]*CC_size[i][0]
        T.append(t)
    for i in range(m):
        if i == 0:
            Eq.append(T[i] == binomial(n,k) -1)  ######## maximum eigenvalue
            #print Eq[-1]
        elif M[i] == [n-i,i]:
            Eq.append(T[i] == -binomial(n,i) + binomial(n,i-1))
    print "\n\n"
    Sol = solve(Eq,V)
    print Eq
    print "Weight possible?", len(Sol) != 0
    if len(Sol) == 0:
        return sys.exit()
##################################################################
    G = SymmetricGroup(n)
    derangement_conjugacy_classes_representatives = []
    CC = G.conjugacy_classes_representatives()
    for x in CC:
        if is_derangement(x,P) == True:
            derangement_conjugacy_classes_representatives.append(x)
    Q = derangement_conjugacy_classes_representatives
    W = [0]*len(Q)
    for i in range(len(Q)):
        u = Q[i].cycle_type()
        for j in range(l):
            if u == Partition(L[j]):
                W[i] = Sol[0][j].rhs()
    print "\n",Sol,"\n"
    return [Sol[0][i].rhs() for i in range(len(Sol[0]))],W,Q

def find_table(m,L,M):
    T = []
    G = SymmetricGroup(m)
    Irr = G.irreducible_characters()
    P = Partitions(m)
    CC = G.conjugacy_classes_representatives()
    C = []
    for x in L:
     for y in CC:
         if x == list(y.cycle_type()):
             C.append(y)
    Index = []
    for x in M:
        for i in range(len(P)):
            if Partition(x) == P[i]:
                Index.append(i)
    for i in [len(Irr)-1-j for j in Index]:
        phi = Irr[i]
        T.append([phi(x) for x in C])#,P[len(P)-1-i]
    return T

if is_even(n) == True and n>=10:
    PC = [[n],[n-1,1],[n-2,2],[n-3,3]]
    L = [[n-5,5],[n-6,2,2,2],[n-6,4,1,1]]
    print find_table(n,L,PC)
    W = modified_weights(n,3,L,PC,find_table(n,L,PC))
    L =  eigenvalues_weights_with_conjugacy_classes(n,k,W[1],W[2],key)
    print L

elif n in [7]:#,

        """ ORDER THE CHARACTERS PROPERLY"""
        PC = [[n],[n-1,1],[n-2,2],[n-3,3]] #permutation_characters
        L = [[n],[n-2,1,1]]
        print find_table(n,L,PC)
        W = modified_weights(n,3,L,PC,find_table(n,L,PC))
        print W[1],"\n"
        L =  eigenvalues_weights_with_conjugacy_classes(n,k,W[1],W[2],key)
        print L

elif n == 8:
    """ ORDER THE CHARACTERS PROPERLY"""
    PC = [[n],[n-1,1],[n-2,2],[n-3,3]]
    L = [[7,1],[2,2,2,2]]
    print find_table(n,L,PC)
    W = modified_weights(n,3,L,PC,find_table(n,L,PC))
    L =  eigenvalues_weights_with_conjugacy_classes(n,k,W[1],W[2],key)
    print L

elif is_even(n) == False and n >= 9:

    """ ORDER THE CHARACTERS PROPERLY"""
    PC = [[n],[n-1,1],[n-2,2],[n-3,3]] #permutation_characters
    L = [[n],[n-2,1,1],[n-5,4,1]]
    print find_table(n,L,PC)
    W = modified_weights(n,3,L,PC,find_table(n,L,PC))
    print W[1],"\n"
    L =  eigenvalues_weights_with_conjugacy_classes(n,k,W[1],W[2],key)
    print L
